/**
 * Old length.
 * @author Patinya Yongyai
 * @version 3.03.2015
 *
 */
public enum Length implements Unit{
	
	METER("meter", 1.0),
	KILOMETER("kilometer",1000.0),
	MILE("mile",1609.344),
	FOOT("foot",0.30480),
	WA("wa",2.0);
	private String name;
	private double value;
	
	Length(String name, double value){
		this.name = name;
		this.value = value;
	}

	/**
	 * Convert from one length unit to another length unit.
	 * @param amt is amount for convert
	 * @param unit is unit to convert
	 * @return value after convert
	 */
	public double convertTo(double amt, Unit unit) {
		return (amt*value)/unit.getValue();
	}

	/**
	 * @return value from old length
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * @return unit from old length
	 */
	public String toString(){
		return this.name;
	}
	
	
}
