
/**
 * Application to run program.
 * @author Patinya Yongyai
 * @version 3.03.2015
 *
 */
public class UnitApp {
	/**
	 * 
	 * @param args not used
	 */
	public static void main(String[] args){
		UnitConverter uc = new UnitConverter();
		UnitConverterUI ui = new UnitConverterUI(uc);
	}
}
