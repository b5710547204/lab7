/**
 * Convert value from from one length unit to another length unit.
 * @author Patinya Yongyai
 * @version 3.03.2015
 *
 */
public class UnitConverter {
	/**
	 * 
	 * @param amount is value for convert
	 * @param fromUnit is old unit
	 * @param toUnit is new unit
	 * @return value after convert
	 */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	
	/**
	 * 
	 * @return value of Length class
	 */
	public Unit[] getUnits(){
		return Length.values();
	}

}
