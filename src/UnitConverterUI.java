import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.zip.DataFormatException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This is user interface of unit converter.
 * @author Patinya Yongyai
 * @version 3.03.2015
 *
 */
public class UnitConverterUI extends JFrame implements ActionListener{
	private JTextField inputAmount;
	private JComboBox<Unit> unit1;
	private JLabel equal;
	private JTextField result;
	private JComboBox<Unit> unit2;
	private JButton convert;
	private JButton clear;
	private UnitConverter converter;

	UnitConverterUI( UnitConverter uc ){
		super("Distance Converter");
		this.converter = uc;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
	}

	/**
	 * Compound of UnitConverterUI class.
	 */
	public void initComponents(){
		Container container = getContentPane();
		container.setLayout(new FlowLayout());
		inputAmount = new JTextField(10);
		inputAmount.addActionListener(this);
		Unit[] items1 = Length.values();
		unit1 = new JComboBox<Unit>(items1);
		equal = new JLabel("=");
		result = new JTextField(10);
		result.setEditable(false);
		Unit[] items2 = Length.values();
		unit2 = new JComboBox<Unit>(items2);
		convert = new JButton("Convert!");
		convert.addActionListener(this);
		clear = new JButton("Clear");
		clear.addActionListener(new ClearText());
		container.add(inputAmount);
		container.add(unit1);
		container.add(equal);
		container.add(result);
		container.add(unit2);
		container.add(convert);
		container.add(clear);
		pack();
		setVisible(true);
	}

	/**
	 * Show action from UI.
	 * @param e as ActionEvent
	 */
	public void actionPerformed(ActionEvent e) {
		try{
			double amount = Double.parseDouble(inputAmount.getText());
			if(amount<0)
				throw new DataFormatException();
			String newUnit1 = unit1.getSelectedItem()+"";
			Length fromUnit = Length.valueOf(newUnit1.toUpperCase());
			String newUnit2 = unit2.getSelectedItem()+"";
			Length toUnit = Length.valueOf(newUnit2.toUpperCase());
			result.setText(converter.convert(amount,fromUnit,toUnit)+"");
		}catch(NumberFormatException ex){
			JOptionPane.showMessageDialog(UnitConverterUI.this, "Input only number!!");
			clear.doClick();
		}catch(DataFormatException ex){
			JOptionPane.showMessageDialog(UnitConverterUI.this, "Input more than 0");
			clear.doClick();
		}
	}
	
	public class ClearText implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			inputAmount.setText("");
			result.setText("");
		}
		
	}

}
